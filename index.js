function Pokemon(name,level){
	// properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	// methods
	this.tackle = function(target){

		if(target.health > 5){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " +(target.health - this.attack));
		let newHealth = target.health -this.attack
		target.health = newHealth
		if (newHealth < 5) {
			console.log(target.name + " fainted.")
		}
	}else{
		console.log(target.name + " already fainted.")
	}
	};
	
};

let pikachu = new Pokemon("Pikachu",16);
let charizard = new Pokemon("Charizard",16);

// console.log(charizard);
// pikachu.tackle(charizard);
// pikachu.tackle(charizard);
// pikachu.tackle(charizard);
// pikachu.tackle(charizard);

